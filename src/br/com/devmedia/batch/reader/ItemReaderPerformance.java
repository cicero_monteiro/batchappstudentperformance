package br.com.devmedia.batch.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;
import java.util.Properties;

import javax.batch.api.chunk.AbstractItemReader;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.context.JobContext;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

@Named("itemReaderPerformance")
@Dependent
public class ItemReaderPerformance extends AbstractItemReader {
	
	@Inject
	protected JobContext JobContext;
	private FileReader fileReader;
	private BufferedReader bufferedReader;
	private String line;
	private int numberOfRecord;
	
	@Override
	public void open(Serializable checkpoint) throws Exception {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		Properties jobParameters = jobOperator.getParameters(JobContext.getExecutionId());
		String resourceName = jobParameters.getProperty("INPUT_DATA_FILENAME");
		fileReader = new FileReader(resourceName);
		bufferedReader = new BufferedReader(fileReader);
		
		System.out.println("[ItemReaderPerformance] Open file student note from recorded: " + numberOfRecord);
	}

	@Override
	public Object readItem() throws Exception {
		line = bufferedReader.readLine();
		if (line != null)
			numberOfRecord++;
		return line;
	}
	
	@Override
	public void close() throws Exception {
		bufferedReader.close();
		fileReader.close();
		
		System.out.println("[ItemReaderPerformance] Shutdown file executed. Total records read: " + numberOfRecord);
	}

}
