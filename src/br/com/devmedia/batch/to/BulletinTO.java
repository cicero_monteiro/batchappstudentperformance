package br.com.devmedia.batch.to;

import java.io.Serializable;

public class BulletinTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StudentTO studentTO;
	private double finalAverage;
	private String situation;

	public StudentTO getStudentTO() {
		return studentTO;
	}

	public void setStudentTO(StudentTO studentTO) {
		this.studentTO = studentTO;
	}

	public double getFinalAverage() {
		return finalAverage;
	}

	public void setFinalAverage(double finalAverage) {
		this.finalAverage = finalAverage;
	}

	public String getSituation() {
		return situation;
	}

	public void setSituation(String situation) {
		this.situation = situation;
	}

}
