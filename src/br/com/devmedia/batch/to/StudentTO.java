package br.com.devmedia.batch.to;

import java.io.Serializable;

public class StudentTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int enrollment;
	private String name;
	private double note1;
	private double note2;
	private double note3;

	public int getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(int enrollment) {
		this.enrollment = enrollment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getNote1() {
		return note1;
	}

	public void setNote1(double note1) {
		this.note1 = note1;
	}

	public double getNote2() {
		return note2;
	}

	public void setNote2(double note2) {
		this.note2 = note2;
	}

	public double getNote3() {
		return note3;
	}

	public void setNote3(double note3) {
		this.note3 = note3;
	}

}
