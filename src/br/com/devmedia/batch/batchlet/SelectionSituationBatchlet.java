package br.com.devmedia.batch.batchlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.batch.api.AbstractBatchlet;
import javax.enterprise.context.Dependent;
import javax.inject.Named;

import br.com.devmedia.batch.interfaces.ParameterInterface;

@Named("selectionBatchlet")
@Dependent
public class SelectionSituationBatchlet extends AbstractBatchlet {
	
	private FileWriter fileWriter;
	private FileReader fileReader;
	private PrintWriter printWriter;
	private BufferedReader bufferedReader;
	private String response;

	@Override
	public String process() throws Exception {
		processApproved();
		processDesapproved();
		removeIntermediateFile();
		
		System.out.println("[SelectionSituationBatchlet] Files the approved and desapproved recorded.");
		
		return response;
	}

	private void processApproved() {
		try {
			// read file generated in step1
			fileReader = new FileReader(ParameterInterface.OUTPUT_DATA_FILENAME);
			bufferedReader = new BufferedReader(fileReader);
			
			// configure file to recorded students approved
			fileWriter = new FileWriter(ParameterInterface.OUTPUT_APPROVED_DATA_FILENAME);
			printWriter = new PrintWriter(fileWriter);
			
			String record;
			String[] fields;
			
			while ((record = bufferedReader.readLine()) != null) {
				fields = record.split(";");
				if (fields[fields.length - 1].equals(ParameterInterface.APPROVED))
					printWriter.println(record);
			}
			
			response = "OK";
			
		} catch (IOException e) {
			System.out.println("Error in the recording of approved file: " + e.getMessage());
			response = "NOK";
		} finally {
			try {
				printWriter.close();
				fileWriter.close();
				bufferedReader.close();
				fileReader.close();
			} catch (IOException e) {
				System.out.println("Error in the shutdown files: " + e.getMessage());
				response = "NOK";
			}
		}
	}
	
	private void processDesapproved() {
		try {
			// read file generated in step1
			fileReader = new FileReader(ParameterInterface.OUTPUT_DATA_FILENAME);
			bufferedReader = new BufferedReader(fileReader);
			
			// configure file to recorded students approved
			fileWriter = new FileWriter(ParameterInterface.OUTPUT_DISAPPROVED_DATA_FILENAME);
			printWriter = new PrintWriter(fileWriter);
			
			String record;
			String[] fields;
			
			while ((record = bufferedReader.readLine()) != null) {
				fields = record.split(";");
				if (fields[fields.length - 1].equals(ParameterInterface.DISAPPROVED))
					printWriter.println(record);
			}
			
			response = "OK";
			
		} catch (IOException e) {
			System.out.println("Error in the recording of disapproved file: " + e.getMessage());
			response = "NOK";
		} finally {
			try {
				printWriter.close();
				fileWriter.close();
				bufferedReader.close();
				fileReader.close();
			} catch (IOException e) {
				System.out.println("Error in the shutdown files: " + e.getMessage());
				response = "NOK";
			}
		}
	}
	
	private void removeIntermediateFile() {
		File intermediateFile = new File(ParameterInterface.OUTPUT_DATA_FILENAME);
		intermediateFile.delete();
	}
}
