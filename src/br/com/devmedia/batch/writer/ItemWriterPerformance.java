package br.com.devmedia.batch.writer;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Properties;

import javax.batch.api.chunk.AbstractItemWriter;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.context.JobContext;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.devmedia.batch.to.BulletinTO;

@Named("itemWriterPerformance")
@Dependent
public class ItemWriterPerformance extends AbstractItemWriter {
	
	@Inject
	protected JobContext jobContext;
	private FileWriter fileWriter;
	private PrintWriter printWriter;
	
	@Override
	public void writeItems(@SuppressWarnings("rawtypes") List items) throws Exception {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		Properties jobParameters = jobOperator.getParameters(jobContext.getExecutionId());
		String resourceName = jobParameters.getProperty("OUTPUT_DATA_FILENAME");
		
		fileWriter = new FileWriter(resourceName, true);
		printWriter = new PrintWriter(fileWriter);
		
		BulletinTO bulletinTO;
		String record;
		DecimalFormat df = new DecimalFormat("#0.00");
		
		for (int i = 0; i < items.size(); i++) {
			Object obj = items.get(i);
			bulletinTO = (BulletinTO) obj;
			record = (bulletinTO.getStudentTO().getEnrollment() + ";" + bulletinTO.getStudentTO().getName() +
					";" + df.format(bulletinTO.getFinalAverage()) + ";" + bulletinTO.getSituation());
			
			printWriter.println(record);
		}
		
		fileWriter.close();
		printWriter.close();
		
		System.out.println("[ItemWriterPerformance] Items recorded.");
	}

}
