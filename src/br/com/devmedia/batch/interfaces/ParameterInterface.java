package br.com.devmedia.batch.interfaces;

public interface ParameterInterface {
	
	// file name XML JSL do not extension ".xml"
	public static final String JOB_NAME = "jobPerformance";
	
	public static final String INPUT_DATA_FILENAME = "/home/leandro/performanceFile/fileStudentsNote.csv";
	public static final String OUTPUT_DATA_FILENAME = "/home/leandro/performanceFile/fileStudentsPerformance.csv";
	
	public static final String OUTPUT_APPROVED_DATA_FILENAME = "/home/leandro/performanceFile/fileStudentsApproved.csv";
	public static final String OUTPUT_DISAPPROVED_DATA_FILENAME = "/home/leandro/performanceFile/fileStudentsDesapproved.csv";

	public static final String APPROVED = "Approved";
	public static final String DISAPPROVED = "Disapproved";

}
