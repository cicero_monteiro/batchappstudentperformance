package br.com.devmedia.batch.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.devmedia.batch.interfaces.ParameterInterface;
import br.com.devmedia.batch.to.JobInformationTO;

@ManagedBean
@ViewScoped
public class ExecutorMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<JobInformationTO> jobs;
	
	@PostConstruct
	private void init() {
		jobs = new ArrayList<>();
	}
	
	public void calculatePerformance(ActionEvent event) {
		try {
			long jobExecutionID = startNewBatchJob();
			
			JobOperator jobOperator = BatchRuntime.getJobOperator();
			JobExecution jobExecution = jobOperator.getJobExecution(jobExecutionID);
			
			JobInformationTO job = new JobInformationTO();
			job.setInstanceID(jobOperator.getJobInstance(jobExecutionID).getInstanceId());
			job.setExecutionID(jobExecutionID);
			job.setJobName(jobExecution.getJobName());
			job.setStartTime(jobExecution.getStartTime());
			
			jobs.add(job);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateExecutionStatus(ActionEvent event) {
		List<JobInformationTO> jobsNew = new ArrayList<>();
		
		for (JobInformationTO job : this.jobs) {
			if (job.getEndTime() == null && job.getBatchStatus() == null) {
				job.setEndTime(BatchRuntime.getJobOperator().getJobExecution(job.getExecutionID()).getEndTime());
				job.setBatchStatus(BatchRuntime.getJobOperator().getJobExecution(job.getExecutionID()).getBatchStatus().name());
			}
			jobsNew.add(job);
		}
		this.jobs = jobsNew;
	}

	private long startNewBatchJob() {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		Properties props = new Properties();
		props.setProperty("INPUT_DATA_FILENAME", ParameterInterface.INPUT_DATA_FILENAME);
		props.setProperty("OUTPUT_DATA_FILENAME", ParameterInterface.OUTPUT_DATA_FILENAME);
		props.setProperty("OUTPUT_APPROVED_DATA_FILENAME", ParameterInterface.OUTPUT_APPROVED_DATA_FILENAME);
		props.setProperty("OUTPUT_DISAPPROVED_DATA_FILENAME", ParameterInterface.OUTPUT_DISAPPROVED_DATA_FILENAME);
		return jobOperator.start(ParameterInterface.JOB_NAME, props);
	}

	public List<JobInformationTO> getJobs() {
		return jobs;
	}

	public void setJobs(List<JobInformationTO> jobs) {
		this.jobs = jobs;
	}
}
