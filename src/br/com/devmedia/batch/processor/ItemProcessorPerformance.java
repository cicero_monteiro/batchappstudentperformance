package br.com.devmedia.batch.processor;

import javax.batch.api.chunk.ItemProcessor;
import javax.enterprise.context.Dependent;
import javax.inject.Named;

import br.com.devmedia.batch.interfaces.ParameterInterface;
import br.com.devmedia.batch.to.BulletinTO;
import br.com.devmedia.batch.to.StudentTO;

@Named("itemProcessorPerformance")
@Dependent
public class ItemProcessorPerformance implements ItemProcessor {

	@Override
	public Object processItem(Object item) throws Exception {
		StudentTO studentTO = new StudentTO();
		String[] fields = item.toString().split(";");
		studentTO.setEnrollment(Integer.parseInt(fields[0]));
		studentTO.setName(fields[1]);
		studentTO.setNote1(Double.parseDouble(fields[2]));
		studentTO.setNote2(Double.parseDouble(fields[3]));
		studentTO.setNote3(Double.parseDouble(fields[4]));
		
		System.out.println("[ItemProcessorPerformance] Item Processed.");
		
		return calculateStudentPerformance(studentTO);
	}

	private Object calculateStudentPerformance(StudentTO studentTO) {
		BulletinTO bulletinTO = new BulletinTO();
		bulletinTO.setStudentTO(studentTO);
		bulletinTO.setFinalAverage((studentTO.getNote1() + studentTO.getNote2() + studentTO.getNote3()) / 3);
		if (bulletinTO.getFinalAverage() < 6) {
			bulletinTO.setSituation(ParameterInterface.DISAPPROVED);
		} else {
			bulletinTO.setSituation(ParameterInterface.APPROVED);
		}
		return bulletinTO;
	}

}
